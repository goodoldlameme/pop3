from client import *
import sys

def main():
    client = POP3Client()
    while True:
        command = sys.stdin.readline().rstrip()
        if command.startswith('login'):
            print(client.user(command.split()[1]))
        if command.startswith('pass'):
            print(client.password(command.split()[1]))
        if command.startswith('stat'):
            print(client.stat())
        if command.startswith('top'):
            print(client.top(command.split()[1], command.split()[2]))
        if command.startswith('from'):
            print(client.head(command.split()[1], 'From'))
        if command.startswith('to'):
            print(client.head(command.split()[1], 'To'))
        if command.startswith('date'):
            print(client.head(command.split()[1], 'Date'))
        if command.startswith('subject'):
            print(client.head(command.split()[1], 'Subject'))
        if command.startswith('full'):
            print(client.full(command.split()[1]))
        if command.startswith('list'):
            print(client.list(command.split()[1] if len(command)>4 else None))
        if command.startswith('retr'):
            print(client.retr(command.split()[1]))
        if command.startswith('quit'):
            print(client.quit())
            break

if __name__ == "__main__":
    main()